# OpenMC on Merlin

Written by Arnau Albà (AMAS, LSM) in March 2023.

Getting OpenMC to run on Merlin is not straight-forward. Here are the steps, which are based mostly on the excellent [instructions](https://docs.openmc.org/en/stable/usersguide/install.html) provided by OpenMC.

The cleanest way to do the installation is to use a conda environment.

## Making the conda environment
```
module load anaconda
conda create --p /path/to/some/directory/env_for_openmc python=3.8
conda activate /path/to/env_for_openmc
conda install numpy
conda install ipykernel
conda deactivate
```

The `numpy` package is necessary for setting up OpenMC later on, and `ipykernel` is necessary for using the environemnt `env_for_openmc` on JupyterHub.

## Building from source
Start by loading the necessary modules:
```
module use unstable
module load cmake/3.20.5
module load gcc/10.3.0
module load hdf5_serial/1.10.7
```
The CMake and gcc versions are probably not too important, but for the HDF5 library, it is important that it is a "serial" one. I encountered segmentation faults when running the version compiled with "HDF5 parallel".

Now clone the OpenMC repository and build:
```
git clone --recurse-submodules https://github.com/openmc-dev/openmc.git
cd openmc
git checkout master
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/path/to/env_for_openmc ../
make
make install
```
Note the "PREFIX" flag, which ensures that OpenMC is installed in `env_for_openmc/bin`. This is necessary later on for using the Python API. Verify that the installation was done correctly with
```
conda activate /path/to/env_for_openmc && which openmc && conda deactivate
```
which should return the location of the OpenMC binary.


## Installing the Python API
```
cd /path/to/openmc
conda activate /path/to/env_for_openmc
pip install .
conda deactivate
```

Now you can call OpenMC from within Python. Test this by ensuring that the following commands work without error:
```
conda activate /path/to/env_for_openmc
python
import openmc
import openmc.deplete
conda deactivate
```

## Nuclear Data and Decay Chains
OpenMC uses nuclear data files in HDF5 format. They are generated simply by transforming the ACE files like the ones from MCNP or Serpent into HDF5 format. The developers provide some [scripts](https://github.com/openmc-dev/data) to do this.

Alternatively, you can download the provided libraries from the [OpenMC website](https://openmc.org/official-data-libraries/), which are already in HDF5 format.

Whether you download the library or generate it with the scripts, you will be left with several `.h5` files and one file called `cross_sections.xml`. This latter file needs to be added as an environment variable, with the following command
```
export OPENMC_CROSS_SECTIONS="/path/to/cross/section/files/nndc-b7.1-hdf5/cross_sections.xml
```

Finally, if you will be carrying out depletion calculations, you also require a "decay chain" file. Here again you can [download](https://openmc.org/depletion-chains/) one, or use the provided scripts to generate one from endfb files.


## JupyterHub
Since OpenMC has a very nice Python API, you might want to use it on Jupyter notebooks. On Merlin, you can use Jupyter notebooks with [JupyterHub](https://merlin-jupyter.psi.ch:8000/hub/login?next=%2Fhub%2Fspawn). Choose the kernel called `env_for_openmc`, which should appear in the kernel list if you did everything correctly.

# Examples

Several examples are provided in the OpenMC [documentation](https://github.com/openmc-dev/openmc/wiki/Example-Jupyter-Notebooks). In this repo there is an additional example for modelling spent fuel in a canister.
