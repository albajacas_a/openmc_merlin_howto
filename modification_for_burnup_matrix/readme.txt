When running depletion calculations, we sometimes want to store the depletion matrix A, as well as the timestep dt and initial isotopic concentration n0.
By default, openmc does not store these. By modifying the file in openmc/deplete/cram.py this can be achieved. The modified file cram.py is provided
in this directory.